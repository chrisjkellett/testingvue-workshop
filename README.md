# vue testing workshop

## Project setup

- npm install to install
- npm test to run test watcher
- npm run serve to run the app

# Details

- This repo was created using vue-cli tool with @vue-test-utils and jest
- The jest config in package.json was amended slightly so that we can run tests from anywhere in the app, not just from a dedicated
test file, which is the default when setting up with the cli.
